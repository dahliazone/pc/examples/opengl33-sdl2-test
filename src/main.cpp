/*

  Simple GLSL 1.5 Demo

  www.lighthouse3d.com

*/

#include <cstdio>
#include <cstdlib>
#include <memory.h>
#include <cmath>
#include <iostream>
#include <sstream>
#include <stdexcept>

//#if defined (__APPLE_CC__)
//#include <OpenGL/gl3.h>
//#else
//#include "GL/glew.h"
//#endif
//#include "GL/freeglut.h"
#if defined (__APPLE_CC__)
#include <OpenGL/gl3.h>
#include <SDL2/SDL.h>
#else
#include "GL/glew.h"
#include "SDL2/SDL.h"
#include "SDL2/SDL_opengl.h"
#endif


SDL_Window *window;
SDL_GLContext context;
const int width = 320;
const int height = 320;

//EventListener *listener;
unsigned last_frame;


#include "textfile.h"

//#define M_PI       3.14159265358979323846

// Data for drawing Axis
float verticesAxis[] = {-20.0f, 0.0f, 0.0f, 1.0f,
			20.0f, 0.0f, 0.0f, 1.0f,

			0.0f, -20.0f, 0.0f, 1.0f,
			0.0f,  20.0f, 0.0f, 1.0f,

			0.0f, 0.0f, -20.0f, 1.0f,
			0.0f, 0.0f,  20.0f, 1.0f};

float colorAxis[] = {   0.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 0.0f};

// Data for triangle 1
float vertices1[] = {   -3.0f, 0.0f, -5.0f, 1.0f,
			-1.0f, 0.0f, -5.0f, 1.0f,
			-2.0f, 2.0f, -5.0f, 1.0f};

float colors1[] = { 0.0f, 0.0f, 1.0f, 1.0f,
			0.0f, 0.0f, 1.0f, 1.0f,
			0.0f,0.0f, 1.0f, 1.0f};

// Data for triangle 2
float vertices2[] = {   1.0f, 0.0f, -5.0f, 1.0f,
			3.0f, 0.0f, -5.0f, 1.0f,
			2.0f, 2.0f, -5.0f, 1.0f};

float colors2[] = { 1.0f, 0.0f, 0.0f, 1.0f,
			1.0f, 0.0f, 0.0f, 1.0f,
			1.0f,0.0f, 0.0f, 1.0f};

// Shader Names
const char *vertexFileName = "data/color.vert";
const char *fragmentFileName = "data/color.frag";

// Program and Shader Identifiers
GLuint p,v,f;

// Vertex Attribute Locations
GLuint vertexLoc, colorLoc;

// Uniform variable Locations
GLuint projMatrixLoc, viewMatrixLoc;

// Vertex Array Objects Identifiers
GLuint vao[3];

// storage for Matrices
float projMatrix[16];
float viewMatrix[16];

// ----------------------------------------------------
// VECTOR STUFF
//

// res = a cross b;
void crossProduct( float *a, float *b, float *res) {

	res[0] = a[1] * b[2]  -  b[1] * a[2];
	res[1] = a[2] * b[0]  -  b[2] * a[0];
	res[2] = a[0] * b[1]  -  b[0] * a[1];
}

// Normalize a vec3
void normalize(float *a) {

	float mag = sqrt(a[0] * a[0]  +  a[1] * a[1]  +  a[2] * a[2]);

	a[0] /= mag;
	a[1] /= mag;
	a[2] /= mag;
}

// ----------------------------------------------------
// MATRIX STUFF
//

// sets the square matrix mat to the identity matrix,
// size refers to the number of rows (or columns)
void setIdentityMatrix( float *mat, int size) {

	// fill matrix with 0s
	for (int i = 0; i < size * size; ++i)
			mat[i] = 0.0f;

	// fill diagonal with 1s
	for (int i = 0; i < size; ++i)
		mat[i + i * size] = 1.0f;
}

//
// a = a * b;
//
void multMatrix(float *a, float *b) {

	float res[16];

	for (int i = 0; i < 4; ++i) {
		for (int j = 0; j < 4; ++j) {
			res[j*4 + i] = 0.0f;
			for (int k = 0; k < 4; ++k) {
				res[j*4 + i] += a[k*4 + i] * b[j*4 + k];
			}
		}
	}
	memcpy(a, res, 16 * sizeof(float));

}

// Defines a transformation matrix mat with a translation
void setTranslationMatrix(float *mat, float x, float y, float z) {

	setIdentityMatrix(mat,4);
	mat[12] = x;
	mat[13] = y;
	mat[14] = z;
}

// ----------------------------------------------------
// Projection Matrix
//

void buildProjectionMatrix(float fov, float ratio, float nearP, float farP) {

	float f = 1.0f / (float)tan (fov * ((float)M_PI / 360.0f));

	setIdentityMatrix(projMatrix,4);

	projMatrix[0] = f / ratio;
	projMatrix[1 * 4 + 1] = f;
	projMatrix[2 * 4 + 2] = (farP + nearP) / (nearP - farP);
	projMatrix[3 * 4 + 2] = (2.0f * farP * nearP) / (nearP - farP);
	projMatrix[2 * 4 + 3] = -1.0f;
	projMatrix[3 * 4 + 3] = 0.0f;
}

// ----------------------------------------------------
// View Matrix
//
// note: it assumes the camera is not tilted,
// i.e. a vertical up vector (remmeber gluLookAt?)
//

void setCamera(float posX, float posY, float posZ,
			   float lookAtX, float lookAtY, float lookAtZ) {

	float dir[3], right[3], up[3];

	up[0] = 0.0f;   up[1] = 1.0f;   up[2] = 0.0f;

	dir[0] =  (lookAtX - posX);
	dir[1] =  (lookAtY - posY);
	dir[2] =  (lookAtZ - posZ);
	normalize(dir);

	crossProduct(dir,up,right);
	normalize(right);

	crossProduct(right,dir,up);
	normalize(up);

	float aux[16];

	viewMatrix[0]  = right[0];
	viewMatrix[4]  = right[1];
	viewMatrix[8]  = right[2];
	viewMatrix[12] = 0.0f;

	viewMatrix[1]  = up[0];
	viewMatrix[5]  = up[1];
	viewMatrix[9]  = up[2];
	viewMatrix[13] = 0.0f;

	viewMatrix[2]  = -dir[0];
	viewMatrix[6]  = -dir[1];
	viewMatrix[10] = -dir[2];
	viewMatrix[14] =  0.0f;

	viewMatrix[3]  = 0.0f;
	viewMatrix[7]  = 0.0f;
	viewMatrix[11] = 0.0f;
	viewMatrix[15] = 1.0f;

	setTranslationMatrix(aux, -posX, -posY, -posZ);

	multMatrix(viewMatrix, aux);
}

// ----------------------------------------------------

void sdldie(const char *msg)
{
	printf("%s: %s\n", msg, SDL_GetError());
	SDL_Quit();
	exit(1);
}


#ifdef DEBUG
#	ifdef _WIN32
#		define GL_CHECK_FOR_ERRORS(error, otherFilenames)	CheckForErrors(error, __FILE__, __LINE__, __FUNCTION__, otherFilenames)
		// On MSVC++ __PRETTY_FUNCTION__ is either __FUNCTION__, __FUNCDNAME__ or __FUNCSIG__
#	else
#		define GL_CHECK_FOR_ERRORS(error, otherFilenames)	CheckForErrors(error, __FILE__, __LINE__, __PRETTY_FUNCTION__, otherFilenames)
#	endif
#else
#	define GL_CHECK_FOR_ERRORS(error, otherFilenames) 1==1;
#endif

void CheckForErrors(const std::string& message, const std::string& file, int line, const std::string& method, const std::string& otherFilenames)
{
	GLenum e = glGetError();
	if (e != GL_NO_ERROR) {

		std::string msg;
		if (e == GL_INVALID_ENUM)
			msg = "GL_INVALID_ENUM: An unacceptable value is specified for an enumerated argument. The offending command is ignored and has no other side effect than to set the error flag.";
		else if (e == GL_INVALID_VALUE)
			msg = "GL_INVALID_VALUE: A numeric argument is out of range. The offending command is ignored and has no other side effect than to set the error flag.";
		else if (e == GL_INVALID_OPERATION)
			msg = "GL_INVALID_OPERATION: The specified operation is not allowed in the current state. The offending command is ignored and has no other side effect than to set the error flag.";
#if !defined (__APPLE_CC__)
		else if (e == GL_STACK_OVERFLOW)
			msg = "GL_STACK_OVERFLOW: This command would cause a stack overflow. The offending command is ignored and has no other side effect than to set the error flag.";
		else if (e == GL_STACK_UNDERFLOW)
			msg = "GL_STACK_UNDERFLOW: This command would cause a stack underflow. The offending command is ignored and has no other side effect than to set the error flag.";
		else if (e == GL_TABLE_TOO_LARGE)
			msg = "GL_TABLE_TOO_LARGE: The specified table exceeds the implementation's maximum supported table size. The offending command is ignored and has no other side effect than to set the error flag.";
#endif
		else if (e == GL_OUT_OF_MEMORY)
			msg = "GL_OUT_OF_MEMORY: There is not enough memory left to execute the command. The state of the GL is undefined, except for the state of the error flags, after this error is recorded.";

		std::string error = "OpenGL error: "+msg+"\n";
		error += "  Message: "+message+"\n";
		error += "  in: \""+file+"\" at line ";
		std::ostringstream t;
		t << line;
		std::string tStr = t.str();
		error += tStr+"\n";
		error += "  "+method+"\n";
		if (otherFilenames.empty() == false)
			error += "  Dynamic ID: "+otherFilenames;
		//error += "  - GLU error: "
		//StringAdd(error, gluErrorString(e));
		//error += "\n";
		std::cerr << error << std::endl << std::endl;
		std::cerr << "EXITING ABRUPTLY BECAUSE OF OPENGL ERROR!" << std::endl;
		throw std::runtime_error(error.c_str());
		//exit(1);
	}
}

void PrintGlInfo(bool printExtensions)
{
	const GLubyte* renderer = glGetString(GL_RENDERER);
	GL_CHECK_FOR_ERRORS("", "");
	const GLubyte* vendor = glGetString(GL_VENDOR);
	GL_CHECK_FOR_ERRORS("", "");
	const GLubyte* version = glGetString(GL_VERSION);
	GL_CHECK_FOR_ERRORS("", "");
	const GLubyte* glslVersion = glGetString(GL_SHADING_LANGUAGE_VERSION);
	GL_CHECK_FOR_ERRORS("", "");

	GLint major, minor;
	glGetIntegerv(GL_MAJOR_VERSION, &major);
	GL_CHECK_FOR_ERRORS("", "");
	glGetIntegerv(GL_MINOR_VERSION, &minor);
	GL_CHECK_FOR_ERRORS("", "");

	std::cout << "OpenGL vendor:   "<<vendor << std::endl;
	std::cout << "OpenGL renderer: "<<renderer << std::endl;
	std::cout << "OpenGL version:  "<<major<<"."<<minor<<" ("<<version <<")" << std::endl;
	std::cout << "GLSL version:    "<<glslVersion << std::endl;

	if (printExtensions) {
		GLint count;
		glGetIntegerv(GL_NUM_EXTENSIONS, &count);
		GL_CHECK_FOR_ERRORS("", "");
		for (GLint i=0; i<count; i++) {
			std::cout << glGetStringi(GL_EXTENSIONS, i) << std::endl;
		}
	}
}

// ----------------------------------------------------

void changeSize(int w, int h) {

	float ratio;
	// Prevent a divide by zero, when window is too short
	// (you cant make a window of zero width).
	if(h == 0)
		h = 1;

	// Set the viewport to be the entire window
	glViewport(0, 0, w, h);
	GL_CHECK_FOR_ERRORS("", "");

	ratio = (1.0f * w) / h;
	buildProjectionMatrix(53.13f, ratio, 1.0f, 30.0f);
}

void setupBuffers() {

	GLuint buffers[2];

	glGenVertexArrays(3, vao);
	GL_CHECK_FOR_ERRORS("", "");
	//
	// VAO for first triangle
	//
	glBindVertexArray(vao[0]);
	GL_CHECK_FOR_ERRORS("", "");
	// Generate two slots for the vertex and color buffers
	glGenBuffers(2, buffers);
	GL_CHECK_FOR_ERRORS("", "");
	// bind buffer for vertices and copy data into buffer
	glBindBuffer(GL_ARRAY_BUFFER, buffers[0]);
	GL_CHECK_FOR_ERRORS("", "");
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices1), vertices1, GL_STATIC_DRAW);
	GL_CHECK_FOR_ERRORS("", "");
	glEnableVertexAttribArray(vertexLoc);
	GL_CHECK_FOR_ERRORS("", "");
	glVertexAttribPointer(vertexLoc, 4, GL_FLOAT, 0, 0, 0);
	GL_CHECK_FOR_ERRORS("", "");

	// bind buffer for colors and copy data into buffer
	glBindBuffer(GL_ARRAY_BUFFER, buffers[1]);
	GL_CHECK_FOR_ERRORS("", "");
	glBufferData(GL_ARRAY_BUFFER, sizeof(colors1), colors1, GL_STATIC_DRAW);
	GL_CHECK_FOR_ERRORS("", "");
	glEnableVertexAttribArray(colorLoc);
	GL_CHECK_FOR_ERRORS("", "");
	glVertexAttribPointer(colorLoc, 4, GL_FLOAT, 0, 0, 0);
	GL_CHECK_FOR_ERRORS("", "");

	//
	// VAO for second triangle
	//
	glBindVertexArray(vao[1]);
	GL_CHECK_FOR_ERRORS("", "");
	// Generate two slots for the vertex and color buffers
	glGenBuffers(2, buffers);
	GL_CHECK_FOR_ERRORS("", "");

	// bind buffer for vertices and copy data into buffer
	glBindBuffer(GL_ARRAY_BUFFER, buffers[0]);
	GL_CHECK_FOR_ERRORS("", "");
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices2), vertices2, GL_STATIC_DRAW);
	GL_CHECK_FOR_ERRORS("", "");
	glEnableVertexAttribArray(vertexLoc);
	GL_CHECK_FOR_ERRORS("", "");
	glVertexAttribPointer(vertexLoc, 4, GL_FLOAT, 0, 0, 0);
	GL_CHECK_FOR_ERRORS("", "");

	// bind buffer for colors and copy data into buffer
	glBindBuffer(GL_ARRAY_BUFFER, buffers[1]);
	GL_CHECK_FOR_ERRORS("", "");
	glBufferData(GL_ARRAY_BUFFER, sizeof(colors2), colors2, GL_STATIC_DRAW);
	GL_CHECK_FOR_ERRORS("", "");
	glEnableVertexAttribArray(colorLoc);
	GL_CHECK_FOR_ERRORS("", "");
	glVertexAttribPointer(colorLoc, 4, GL_FLOAT, 0, 0, 0);
	GL_CHECK_FOR_ERRORS("", "");

	//
	// This VAO is for the Axis
	//
	glBindVertexArray(vao[2]);
	GL_CHECK_FOR_ERRORS("", "");
	// Generate two slots for the vertex and color buffers
	glGenBuffers(2, buffers);
	GL_CHECK_FOR_ERRORS("", "");
	// bind buffer for vertices and copy data into buffer
	glBindBuffer(GL_ARRAY_BUFFER, buffers[0]);
	GL_CHECK_FOR_ERRORS("", "");
	glBufferData(GL_ARRAY_BUFFER, sizeof(verticesAxis), verticesAxis, GL_STATIC_DRAW);
	GL_CHECK_FOR_ERRORS("", "");
	glEnableVertexAttribArray(vertexLoc);
	GL_CHECK_FOR_ERRORS("", "");
	glVertexAttribPointer(vertexLoc, 4, GL_FLOAT, 0, 0, 0);
	GL_CHECK_FOR_ERRORS("", "");

	// bind buffer for colors and copy data into buffer
	glBindBuffer(GL_ARRAY_BUFFER, buffers[1]);
	GL_CHECK_FOR_ERRORS("", "");
	glBufferData(GL_ARRAY_BUFFER, sizeof(colorAxis), colorAxis, GL_STATIC_DRAW);
	GL_CHECK_FOR_ERRORS("", "");
	glEnableVertexAttribArray(colorLoc);
	GL_CHECK_FOR_ERRORS("", "");
	glVertexAttribPointer(colorLoc, 4, GL_FLOAT, 0, 0, 0);
	GL_CHECK_FOR_ERRORS("", "");

}

void setUniforms() {

	// must be called after glUseProgram
	glUniformMatrix4fv(projMatrixLoc,  1, false, projMatrix);
	GL_CHECK_FOR_ERRORS("", "");
	glUniformMatrix4fv(viewMatrixLoc,  1, false, viewMatrix);
	GL_CHECK_FOR_ERRORS("", "");
}

void renderScene(void) {
	setCamera(10,2,10,0,2,-5);

	glUseProgram(p);
	GL_CHECK_FOR_ERRORS("", "");
	setUniforms();

	glBindVertexArray(vao[0]);
	GL_CHECK_FOR_ERRORS("", "");
	glDrawArrays(GL_TRIANGLES, 0, 3);
	GL_CHECK_FOR_ERRORS("", "");

	glBindVertexArray(vao[1]);
	GL_CHECK_FOR_ERRORS("", "");
	glDrawArrays(GL_TRIANGLES, 0, 3);
	GL_CHECK_FOR_ERRORS("", "");

	glBindVertexArray(vao[2]);
	GL_CHECK_FOR_ERRORS("", "");
	glDrawArrays(GL_LINES, 0, 6);
	GL_CHECK_FOR_ERRORS("", "");
}

void processNormalKeys(unsigned char key, int x, int y) {
	(void)x; (void)y;
	if (key == 27) {
		glDeleteVertexArrays(3,vao);
		GL_CHECK_FOR_ERRORS("", "");
		glDeleteProgram(p);
		GL_CHECK_FOR_ERRORS("", "");
		glDeleteShader(v);
		GL_CHECK_FOR_ERRORS("", "");
		glDeleteShader(f);
		GL_CHECK_FOR_ERRORS("", "");
		exit(0);
	}
}

void printShaderInfoLog(GLuint obj)
{
	int infologLength = 0;
	int charsWritten  = 0;
	char *infoLog;

	glGetShaderiv(obj, GL_INFO_LOG_LENGTH,&infologLength);
	GL_CHECK_FOR_ERRORS("", "");

	if (infologLength > 0)
	{
		infoLog = (char *)malloc(infologLength);
		glGetShaderInfoLog(obj, infologLength, &charsWritten, infoLog);
		GL_CHECK_FOR_ERRORS("", "");
		printf("%s\n",infoLog);
		free(infoLog);
	}
}

void printProgramInfoLog(GLuint obj)
{
	int infologLength = 0;
	int charsWritten  = 0;
	char *infoLog;

	glGetProgramiv(obj, GL_INFO_LOG_LENGTH,&infologLength);
	GL_CHECK_FOR_ERRORS("", "");

	if (infologLength > 0)
	{
		infoLog = (char *)malloc(infologLength);
		glGetProgramInfoLog(obj, infologLength, &charsWritten, infoLog);
		GL_CHECK_FOR_ERRORS("", "");
		printf("%s\n",infoLog);
		free(infoLog);
	}
}

GLuint setupShaders() {

	char *vs = NULL,*fs = NULL;

	GLuint p,v,f;

	v = glCreateShader(GL_VERTEX_SHADER);
	GL_CHECK_FOR_ERRORS("", "");
	f = glCreateShader(GL_FRAGMENT_SHADER);
	GL_CHECK_FOR_ERRORS("", "");

	vs = textFileRead(vertexFileName);
	fs = textFileRead(fragmentFileName);

	const char * vv = vs;
	const char * ff = fs;

	glShaderSource(v, 1, &vv,NULL);
	GL_CHECK_FOR_ERRORS("", "");
	glShaderSource(f, 1, &ff,NULL);
	GL_CHECK_FOR_ERRORS("", "");

	free(vs);free(fs);

	glCompileShader(v);
	GL_CHECK_FOR_ERRORS("", "");
	glCompileShader(f);
	GL_CHECK_FOR_ERRORS("", "");

	printShaderInfoLog(v);
	printShaderInfoLog(f);

	p = glCreateProgram();
	GL_CHECK_FOR_ERRORS("", "");
	glAttachShader(p,v);
	GL_CHECK_FOR_ERRORS("", "");
	glAttachShader(p,f);
	GL_CHECK_FOR_ERRORS("", "");

	glBindFragDataLocation(p, 0, "outputF");
	glLinkProgram(p);
	printProgramInfoLog(p);

	vertexLoc = glGetAttribLocation(p,"position");
	colorLoc = glGetAttribLocation(p, "color");

	projMatrixLoc = glGetUniformLocation(p, "projMatrix");
	viewMatrixLoc = glGetUniformLocation(p, "viewMatrix");

	return(p);
}


bool mainLoop();


int main(int argc, char **argv) {
	(void)argc; (void)argv;
	/*
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
#if defined (__APPLE_CC__)
	glutInitContextVersion(3, 2); // or later versions, core was introduced only with 3.2
	glutInitContextProfile(GLUT_CORE_PROFILE);
#endif
	glutInitWindowPosition(100,100);
	glutInitWindowSize(320,320);
	glutCreateWindow("Lighthouse 3D");

	glutDisplayFunc(renderScene);
	glutIdleFunc(renderScene);
	glutReshapeFunc(changeSize);
	glutKeyboardFunc(processNormalKeys);

#if defined (__APPLE_CC__)
#else
	glewInit();
	if (glewIsSupported("GL_VERSION_3_3"))
		printf("Ready for OpenGL 3.3\n");
	else {
		printf("OpenGL 3.3 not supported\n");
		exit(1);
	}
#endif
	*/

	if (SDL_Init(SDL_INIT_VIDEO) < 0) /* Initialize SDL's Video subsystem */
		sdldie("Unable to initialize SDL"); /* Or die on error */

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, true);
	// I'd like to create a forward-compatible context, but it breaks GLEW
#if defined (__APPLE_CC__)
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
#else
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
#endif
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

	window = SDL_CreateWindow("Skrolli GL",
		SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
		width, height,
		SDL_WINDOW_OPENGL);
	if(!window)
		throw std::runtime_error("Could not open a window");

	context = SDL_GL_CreateContext(window);
	if(!context)
		throw std::runtime_error("Could not create OpenGL context");
	SDL_GL_MakeCurrent(window, context);

	GL_CHECK_FOR_ERRORS("", "");

	int err;
#if defined (__APPLE_CC__)
#else
	glewExperimental = GL_TRUE;
	err = glewInit();
	// ignore errors from buggy glew
	glGetError();
	//GL_CHECK_FOR_ERRORS("", "");

	if (glewIsSupported("GL_VERSION_3_3"))
		printf("Ready for OpenGL 3.3\n");
	else {
		std::ostringstream message;
		message << "GLEW initialization failed: " << glewGetErrorString(err);
		throw std::runtime_error(message.str());
	}
#endif
	GL_CHECK_FOR_ERRORS("", "");

	PrintGlInfo(false);

	glEnable(GL_DEPTH_TEST);
	GL_CHECK_FOR_ERRORS("", "");
	glClearColor(1.0,1.0,1.0,1.0);
	GL_CHECK_FOR_ERRORS("", "");

	std::cout << glGetString(GL_RENDERER)<<"\n"<<glGetString(GL_VERSION)<<"\n";
	p = setupShaders();
	setupBuffers();

	err = glGetError();
	if(err!=GL_NO_ERROR)
	{
		std::ostringstream message;
		message << "OpenGL error during initialization: " << std::hex << err << std::dec;
		//throw runtime_error(message.str());
	}

	changeSize(width, height);

	SDL_ShowWindow(window);

	//glutMainLoop();

	while (mainLoop() == true) {}

	return(0);
}


bool mainLoop()
{
	bool result = true;

	SDL_PumpEvents();
	SDL_Event event;
	while(SDL_PollEvent(&event))
	{
		if(event.type==SDL_QUIT)
		{
			result = false;
			//if(listener)
			//	listener->on_quit();
		}
		else if(event.type==SDL_KEYDOWN)
		{
			//if(listener)
			//	listener->on_key_press(event.key.keysym.sym);
		}
		else if(event.type==SDL_KEYUP)
		{
			//if(listener)
			//	listener->on_key_release(event.key.keysym.sym);
		}
	}

	unsigned current_time = SDL_GetTicks();
	//float since_last_frame = 0;
	//if(last_frame)
	//	since_last_frame = (current_time-last_frame)/1000.0f;
	last_frame = current_time;

	// Clear old contents from framebuffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	renderScene();

	// Make the new contents visible.
	SDL_GL_SwapWindow(window);

	int err = glGetError();
	if(err!=GL_NO_ERROR)
		std::cerr<<"OpenGL error: "<<std::hex<<err << std::dec << std::endl;

	return result;
}


